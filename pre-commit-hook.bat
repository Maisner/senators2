php vendor/phpunit/phpunit/phpunit --configuration tests/phpunit.xml tests
php vendor/phpstan/phpstan/bin/phpstan analyse -l 7 -c phpstan.neon src
php vendor/squizlabs/php_codesniffer/bin/phpcs --standard=ruleset.xml --extensions=php --tab-width=4 -sp src