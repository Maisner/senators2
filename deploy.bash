#!/bin/bash

cd /root/senators2

docker-compose down
sudo git checkout master
sudo git pull

mkdir -p temp
chmod 777 -R temp

mkdir -p log
chmod 777 -R log

docker-compose build
docker-compose up -d

docker exec senators2_php_1 composer install --no-interaction --no-dev
docker exec senators2_php_1 php bin/console nette:cache:purge

docker exec senators2_nodejs_1 npm install
docker exec senators2_nodejs_1 npm run build