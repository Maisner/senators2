const path = require('path');
const webpack = require('webpack');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

module.exports = {
	mode:         'production',
	entry:        './src_front/index.js',
	output:       {
		filename: 'bundle.js',
		path:     path.resolve(__dirname, 'public')
	},
	watchOptions: {
		ignored:          /node_modules/,
		aggregateTimeout: 300,
		poll:             500
	},
	module:       {
		rules: [
			{
				test: /\.css$/,
				use:  [
					{
						loader: MiniCssExtractPlugin.loader
					},
					"css-loader"
				]
			}
		],

	},
	plugins:      [
		new webpack.ProvidePlugin({
			$:               'jquery',
			jQuery:          'jquery',
			"window.jQuery": 'jquery',
			"window.Nette":  'nette-forms',
			// "window.Tether": 'tether'
		}),
		new MiniCssExtractPlugin({
			filename:      "[name].css",
			chunkFilename: "[id].css"
		})
	]
};