<?php

namespace Tests\Model\Cart;

use App\Model\ORM\User\User;
use App\Model\ORM\User\UserRepository;
use App\Model\Security\Authenticator;
use Nette\InvalidArgumentException;
use Nette\Security\AuthenticationException;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class AuthenticatorTest extends TestCase {

	/** @var MockObject */
	private $userMock;

	/** @var MockObject */
	private $userRepositoryMock;

	protected function setUp() {
		$userMock = $this->createMock(User::class);
		$userMock->method('getId')->willReturn(5);
		//hash for password: 'secretPswd'
		$userMock->method('getPassword')->willReturn('$2y$10$VtB2lVv9it.jm8dW4AATyOxMxem1tXxlj5dkj7Evz0YXsYbGJB6VC');
		$userMock->method('getRole')->willReturn(User::ROLE_REGISTERED);
		$this->userMock = $userMock;

		$this->userRepositoryMock = $this->getMockBuilder(UserRepository::class)
			->disableOriginalConstructor()
			->getMock();
	}

	public function testAuthenticateCorrectUser() {
		$this->userRepositoryMock->method('findOneBy')->willReturn($this->userMock);

		$credentials = [
			'John',
			'secretPswd'
		];

		$authenticator = new Authenticator($this->userRepositoryMock);

		$identity = $authenticator->authenticate($credentials);

		$this->assertSame(5, $identity->getId());
		$this->assertSame([User::ROLE_REGISTERED], $identity->getRoles());
	}

	public function testAuthenticateNotExistUser() {
		$this->expectException(AuthenticationException::class);

		$this->userRepositoryMock->method('findOneBy')->willReturn(NULL);

		$credentials = [
			'John',
			'secretPswd'
		];

		$authenticator = new Authenticator($this->userRepositoryMock);
		$authenticator->authenticate($credentials);
	}

	public function testAuthenticateInvalidUserPassword() {
		$this->expectException(AuthenticationException::class);

		$this->userRepositoryMock->method('findOneBy')->willReturn($this->userMock);

		$credentials = [
			'John',
			'badPassword'
		];

		$authenticator = new Authenticator($this->userRepositoryMock);
		$authenticator->authenticate($credentials);
	}

	public function testAuthenticateInvalidCredentials() {
		$this->expectException(InvalidArgumentException::class);

		$this->userRepositoryMock->method('findOneBy')->willReturn($this->userMock);

		$credentials = [];

		$authenticator = new Authenticator($this->userRepositoryMock);
		$authenticator->authenticate($credentials);
	}
}