# Senators2
App for amateur hockey team

Use:
- PHP 7.2
- MySQL
- Docker
- Doctrine
- PHPStan

### Webpack in Node.js docker container

###### Webpack build
```
λ docker exec -it senators2_nodejs_1 npm run build
```

###### Webpack watch
```
λ docker exec -it senators2_nodejs_1 npm run watch
```