<?php

return [
	'parameters' => [
		'db' => [
//			'host' => getenv('MYSQL_HOST'),
			'host' => 'db',
			'name' => getenv('MYSQL_DATABASE'),
			'user' => getenv('MYSQL_USER'),
			'pass' => getenv('MYSQL_PASSWORD'),
		]
	]
];