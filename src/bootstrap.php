<?php declare(strict_types = 1);

require __DIR__ . '/../vendor/autoload.php';

$dotenv = new Dotenv\Dotenv(__DIR__ . '/../');
$dotenv->load();

const SRC_DIR = __DIR__;
const PUBLIC_DIR = __DIR__ . '/../public';

$configurator = new Nette\Configurator;

$configurator->setDebugMode(\strtolower(\getenv('DEBUG') ? : '') === 'true');
$configurator->enableTracy(\SRC_DIR . '/../log');
$configurator->setTimeZone(\getenv('TIMEZONE') ? : 'Europe/Prague');
$configurator->setTempDirectory(\SRC_DIR . '/../temp');
$configurator->addConfig(\SRC_DIR . '/../config/config.neon');
$configurator->addParameters(
	[
		'srcDir'    => \SRC_DIR,
		'publicDir' => \PUBLIC_DIR
	]
);

return $configurator->createContainer();
