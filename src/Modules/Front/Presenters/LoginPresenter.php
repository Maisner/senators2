<?php declare(strict_types = 1);

namespace App\Modules\Front\Presenters;

use App\Modules\Front\Components\LoginForm\ILoginFormControlFactory;
use App\Modules\Front\Components\LoginForm\LoginFormControl;
use App\Utils\FlashMessage;
use Nette\Security\IAuthenticator;

class LoginPresenter extends BasePresenter {

	/** @var ILoginFormControlFactory @inject */
	public $loginFormFactory;

	/** @var IAuthenticator @inject */
	public $authenticator;

	public function renderDefault(): void {
	}

	protected function createComponentLoginForm(): LoginFormControl {
		$control = $this->loginFormFactory->create();

		$control->onSuccess[] = function (LoginFormControl $sender, string $username, string $password): void {
			try {
				$this->authenticator->authenticate(
					[
						$username,
						$password
					]
				);
			} catch (\Nette\Security\AuthenticationException $e) {
				$this->flashMessage('Uživatelské jméno nebo heslo se neshoduje', FlashMessage::ERROR);

				$this->redirect('this');
			}
		};

		return $control;
	}
}
