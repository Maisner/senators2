<?php declare(strict_types = 1);

namespace App\Modules\Front\Presenters;

use App\Utils\FlashMessage;

abstract class SecuredPresenter extends BasePresenter {

	public function startup(): void {
		parent::startup();

		if (!$this->getUser()->isLoggedIn()) {
			$this->flashMessage('Pro pokračování se prosím přihlašte', FlashMessage::INFO);
			$this->redirect(':Front:Login:default');
		}

		if (!$this->getUser()->isAllowed($this->getName(), $this->getAction())) {
			$this->flashMessage('Pro tuto akci nemáte dostatečné oprávnění', FlashMessage::INFO);
			$this->redirect(':Front:Homepage:default');
		}
	}
}