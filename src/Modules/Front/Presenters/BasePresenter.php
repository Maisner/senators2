<?php declare(strict_types = 1);

namespace App\Modules\Front\Presenters;

use Nette\Application\UI\Presenter;
use Nette\Localization\ITranslator;

abstract class BasePresenter extends Presenter {

	/** @var ITranslator @inject */
	public $translator;
}