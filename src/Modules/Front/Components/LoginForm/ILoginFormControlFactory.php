<?php declare(strict_types = 1);

namespace App\Modules\Front\Components\LoginForm;

interface ILoginFormControlFactory {

	public function create(): LoginFormControl;
}