<?php declare(strict_types = 1);

namespace App\Modules\Front\Components\LoginForm;

use AlesWita\FormRenderer\BootstrapV4Renderer;
use App\Utils\Component\BaseControl;
use Nette\Application\UI\Form;
use Nette\Localization\ITranslator;
use Nette\Utils\ArrayHash;

/**
 * Class LoginFormControl
 * @package App\Modules\Front\Components\LoginForm
 * @method onSuccess(LoginFormControl $sender, string $username, string $password)
 */
class LoginFormControl extends BaseControl {

	/** @var array|callable[]|\Closure[] */
	public $onSuccess = [];

	public function __construct(ITranslator $translator) {
		parent::__construct($translator);

		$this->setTemplatePath(__DIR__ . '/default.latte');
	}

	public function render(): void {
		parent::render();

		$this->getTemplate()->render();
	}

	protected function createComponentForm(): Form {
		$form = new Form();
		$form->setRenderer(new BootstrapV4Renderer());
		$form->addText('username', 'Username')
			->setRequired();

		$form->addPassword('password', 'Password')
			->setRequired();

		$form->addSubmit('submit', 'Login');

		$form->onSuccess[] = function (Form $form, ArrayHash $values): void {
			$this->onSuccess($this, $values->username, $values->password);

			$this->baseRedraw();
		};

		return $form;
	}
}