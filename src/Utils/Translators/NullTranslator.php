<?php declare(strict_types = 1);

namespace App\Utils\Translators;

use Nette\Localization\ITranslator;

class NullTranslator implements ITranslator {

	/**
	 * @param mixed $message
	 * @param int   $count
	 * @return string
	 */
	public function translate($message, $count = NULL): string {
		return $message;
	}
}