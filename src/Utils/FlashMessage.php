<?php declare(strict_types = 1);

namespace App\Utils;

class FlashMessage {

	public const SUCCESS = 'success';

	public const ERROR = 'danger';

	public const INFO = 'info';
}