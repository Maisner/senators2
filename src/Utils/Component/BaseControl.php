<?php declare(strict_types = 1);

namespace App\Utils\Component;

use Nette\Application\UI\Control;
use Nette\Application\UI\Presenter;
use Nette\Bridges\ApplicationLatte\Template;
use Nette\Localization\ITranslator;

abstract class BaseControl extends Control {

	/** @var ITranslator */
	protected $translator;

	/** @var string */
	protected $templatePath;

	public function __construct(ITranslator $translator) {
		parent::__construct();

		$this->translator = $translator;
	}

	public function setTemplatePath(string $templatePath): void {
		$this->templatePath = $templatePath;
	}

	public function render(): void {
		/** @var Template $template */
		$template = $this->getTemplate();

		$template->setFile($this->templatePath);
		$template->setTranslator($this->translator);
	}

	protected function baseRedraw(): void {
		if ($this->getPresenter() instanceof Presenter && $this->getPresenter()->isAjax()) {
			$this->redrawControl();

			return;
		}

		$this->redirect('this');
	}

}