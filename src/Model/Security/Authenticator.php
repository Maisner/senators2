<?php declare(strict_types = 1);

namespace App\Model\Security;

use App\Model\ORM\User\User;
use App\Model\ORM\User\UserRepository;
use Nette\Security\AuthenticationException;
use Nette\Security\IAuthenticator;
use Nette\Security\Identity;
use Nette\Security\IIdentity;
use Nette\Security\Passwords;

class Authenticator implements IAuthenticator {

	/** @var UserRepository */
	private $userRepository;

	public function __construct(UserRepository $userRepository) {
		$this->userRepository = $userRepository;
	}

	/**
	 * @param array|string[] $credentials
	 * @return IIdentity
	 * @throws \Nette\InvalidArgumentException
	 * @throws AuthenticationException
	 */
	public function authenticate(array $credentials): IIdentity {
		if (!isset($credentials[1], $credentials[0])) {
			throw new \Nette\InvalidArgumentException('Credentials array is invalid');
		}

		list($username, $password) = $credentials;

		/** @var User|null $user */
		$user = $this->userRepository->findOneBy(['username' => $username]);

		if ($user === NULL) {
			throw new \Nette\Security\AuthenticationException('User not found.', IAuthenticator::IDENTITY_NOT_FOUND);
		}

		if (!Passwords::verify($password, $user->getPassword())) {
			throw new \Nette\Security\AuthenticationException('Invalid password.', IAuthenticator::INVALID_CREDENTIAL);
		}

		return new Identity($user->getId(), $user->getRole());
	}
}