<?php declare(strict_types = 1);

namespace App\Model\ORM;

use Doctrine\ORM\Mapping as ORM;

abstract class BaseEntity {
	/**
	 * @ORM\Id()
	 * @ORM\GeneratedValue()
	 * @ORM\Column(type="integer")
	 * @var int
	 */
	protected $id;

	public function getId(): ?int {
		return $this->id;
	}
}