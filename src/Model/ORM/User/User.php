<?php declare(strict_types = 1);

namespace App\Model\ORM\User;

use App\Model\ORM\BaseEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class User
 * @package App\Entity
 * @ORM\Entity()
 */
class User extends BaseEntity {

	public const ROLE_REGISTERED = 'registered';

	public const ROLE_ADMIN = 'admin';

	/**
	 * @var string
	 * @ORM\Column(type="string", length=50, unique=true)
	 */
	private $username;

	/**
	 * @var string
	 * @ORM\Column(type="string", length=100, unique=true)
	 */
	private $email;

	/**
	 * @var bool
	 * @ORM\Column(type="boolean")
	 */
	private $active = FALSE;

	/**
	 * @var string
	 * @ORM\Column(type="string", length=255)
	 */
	private $password;

	/**
	 * @var \DateTime|null
	 * @ORM\Column(type="datetime")
	 */
	private $lastLogin;

	/**
	 * @var string|null
	 * @ORM\Column(type="string", length=255)
	 */
	private $confirmationToken;

	/**
	 * @var string
	 * @ORM\Column(type="string", length=50)
	 */
	private $role;

	/**
	 * User constructor.
	 * @param string $username
	 * @param string $email
	 * @param string $password
	 */
	public function __construct(string $username, string $email, string $password) {
		$this->username = $username;
		$this->email = $email;
		$this->password = $password;
	}

	public function getUsername(): string {
		return $this->username;
	}

	public function setUsername(string $username): void {
		$this->username = $username;
	}

	public function getEmail(): string {
		return $this->email;
	}

	public function isActive(): bool {
		return $this->active;
	}

	public function setActive(bool $active): void {
		$this->active = $active;
	}

	public function getPassword(): string {
		return $this->password;
	}

	public function setPassword(string $password): void {
		$this->password = $password;
	}

	public function getLastLogin(): ?\DateTime {
		return $this->lastLogin;
	}

	public function setLastLogin(\DateTime $lastLogin): void {
		$this->lastLogin = $lastLogin;
	}

	public function getConfirmationToken(): ?string {
		return $this->confirmationToken;
	}

	public function setConfirmationToken(?string $confirmationToken): void {
		$this->confirmationToken = $confirmationToken;
	}

	public function getRole(): string {
		return $this->role;
	}

	public function setRole(string $role): void {
		$this->role = $role;
	}
}