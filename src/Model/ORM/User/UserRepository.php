<?php declare(strict_types = 1);

namespace App\Model\ORM\User;

use App\Model\ORM\BaseRepository;
use Doctrine\ORM\EntityManager;

class UserRepository extends BaseRepository {

	public function __construct(EntityManager $em) {
		parent::__construct($em, User::class);
	}


}