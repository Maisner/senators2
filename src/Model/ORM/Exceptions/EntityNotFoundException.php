<?php declare(strict_types = 1);

namespace App\Model\ORM\Exceptions;


class EntityNotFoundException extends \Exception {

}