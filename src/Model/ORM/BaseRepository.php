<?php declare(strict_types = 1);

namespace App\Model\ORM;

use App\Model\ORM\Exceptions\EntityNotFoundException;
use App\Model\ORM\Exceptions\SaveEntityException;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Tracy\Debugger;

abstract class BaseRepository extends EntityRepository {

	public function __construct(EntityManager $em, string $class) {
		parent::__construct($em, $em->getClassMetadata($class));
	}

	/**
	 * @param BaseEntity $entity
	 * @return BaseEntity
	 * @throws SaveEntityException
	 */
	public function save(BaseEntity $entity): BaseEntity {
		try {
			if ($entity->getId() === NULL) {
				$this->_em->persist($entity);
			}

			$this->_em->flush();
		} catch (\Throwable $e) {
			Debugger::log($e, Debugger::ERROR);

			throw new \App\Model\ORM\Exceptions\SaveEntityException((string)$e);
		}

		return $entity;
	}

	/**
	 * @param int $id
	 * @return BaseEntity
	 * @throws EntityNotFoundException
	 */
	public function getById(int $id): BaseEntity {
		$entity = $this->find($id);

		if ($entity instanceof BaseEntity) {
			return $entity;
		}

		throw new \App\Model\ORM\Exceptions\EntityNotFoundException(
			"Entity '{$this->_class}' with id '{$id}' not found"
		);
	}

	public function delete(BaseEntity $entity): void {
		$this->_em->remove($entity);
		$this->_em->flush();
	}
}