<?php declare(strict_types = 1);

namespace App\Model\Facades;

use App\Model\ORM\Exceptions\SaveEntityException;
use App\Model\ORM\User\User;
use App\Model\ORM\User\UserRepository;
use Nette\Security\Passwords;
use Nette\Utils\Random;

class Users {

	/** @var UserRepository */
	private $userRepository;

	public function __construct(UserRepository $userRepository) {
		$this->userRepository = $userRepository;
	}

	/**
	 * @param string $email
	 * @param string $username
	 * @param string $password
	 * @return User
	 * @throws SaveEntityException
	 */
	public function registerUser(string $email, string $username, string $password): User {
		//check unique email and username

		$user = new User($username, $email, Passwords::hash($password));
		$user->setActive(FALSE);
		$user->setConfirmationToken($this->generateConfirmationToken());
		$user->setRole(User::ROLE_REGISTERED);

		$this->userRepository->save($user);

		//fire event!!

		return $user;
	}

	protected function generateConfirmationToken(): string {
		return Random::generate(20);
	}
}