<?php declare(strict_types = 1);

namespace App\Router;

use Nette;
use Nette\Application\Routers\Route;
use Nette\Application\Routers\RouteList;


class RouterFactory {
	use Nette\StaticClass;

	public static function createRouter(): Nette\Application\IRouter {
		$router = new RouteList;
		$router[] = new Route('<presenter>/<action>[/<id>]', 'Front:Homepage:default');

		return $router;
	}
}
